# ---------------------------------------------------------------
# Build monkey
# ---------------------------------------------------------------

root-verify:
	@if [ $(UID) -ne 0 ]; then \
		$(MSG11) "You must be root." $(EMSG); \
		exit 1; \
	fi

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

$(MONKEY_T)-verify:
	@if [ "$(CROSS_COMPILER)" = "" ] || [ ! -f $(CROSS_COMPILER) ]; then \
		$(MSG11) "Can't find cross toolchain.  Try setting XI= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(SD)" = "" ] || [ ! -d $(SD) ]; then \
		$(MSG11) "Can't find staging tree.  Try setting SD= on the command line." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
.$(MONKEY_T)-get $(MONKEY_T)-get: 
	@mkdir -p $(BLDDIR) $(ARCDIR)
	@if [ ! -f $(ARCDIR)/$(MONKEY_T) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving MONKEY source" $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(ARCDIR) && git clone $(MONKEY_URL); \
		cd $(ARCDIR)/$(MONKEY_T) && git checkout $(MONKEY_GIT_TAG); \
	else \
		$(MSG3) "MONKEY source is cached" $(EMSG); \
	fi
	@touch .$(subst .,,$@)

.$(MONKEY_T)-get-patch $(MONKEY_T)-get-patch: .$(MONKEY_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
.$(MONKEY_T)-unpack $(MONKEY_T)-unpack: .$(MONKEY_T)-get-patch
	@$(MSG) "================================================================"
	@$(MSG3) "Unpacking MONKEY source" $(EMSG)
	@$(MSG) "================================================================"
	@cp -r $(ARCDIR)/$(MONKEY_T) $(BLDDIR)/
	@mv $(BLDDIR)/$(MONKEY_T) $(MONKEY_SRCDIR)
	@touch .$(subst .,,$@)

# Apply patches
.$(MONKEY_T)-patch $(MONKEY_T)-patch: .$(MONKEY_T)-unpack
	@touch .$(subst .,,$@)

.$(MONKEY_T)-init $(MONKEY_T)-init: 
	@make $(MONKEY_T)-verify
	@make .$(MONKEY_T)-patch
	@touch .$(subst .,,$@)

.$(MONKEY_T)-config $(MONKEY_T)-config: 
	cd $(MONKEY_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
    	CC=arm-unknown-linux-gnueabi-gcc \
    	CFLAGS=-I$(SD)/usr/include \
    	LDFLAGS="-L$(SD)/usr/lib -L$(SD)/lib --sysroot=$(SD)/" \
    	./configure \
        	--prefix=/ \
        	--bindir=/usr/bin \
        	--libdir=/usr/lib/monkey \
        	--incdir=/usr/include \
        	--sysconfdir=/etc/monkey \
        	--datadir=/home/httpd/monkey \
        	--plugdir=/usr/lib \
        	--mandir=/usr/man \
        	--logdir=/var/volatile/log \
        	--enable-plugins=cgi \
        	--safe-free

# Build the package
$(MONKEY_T): .$(MONKEY_T)

.$(MONKEY_T): .$(MONKEY_T)-init 
	@make --no-print-directory $(MONKEY_T)-config
	@$(MSG) "================================================================"
	@$(MSG2) "Building MONKEY" $(EMSG)
	@$(MSG) "================================================================"
	@cd $(MONKEY_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
    		CFLAGS=-I$(SD)/usr/include \
    		LDFLAGS="-L$(SD)/usr/lib -L$(SD)/lib --sysroot=$(SD)/" \
    		DESTDIR=$(MONKEY_BLDDIR) \
    		make 
	@cd $(MONKEY_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
    		CFLAGS=-I$(SD)/usr/include \
    		LDFLAGS="-L$(SD)/usr/lib -L$(SD)/lib --sysroot=$(SD)/" \
    		DESTDIR=$(MONKEY_BLDDIR) \
    		make install
	@touch .$(subst .,,$@)

$(MONKEY_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "MONKEY Build Files" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(MONKEY_BLDDIR)

# Package it as an opkg 
opkg: pkg

pkg: $(MONKEY_T)-pkg

$(MONKEY_T)-pkg: .$(MONKEY_T) 
	@make --no-print-directory root-verify opkg-verify
	@mkdir -p $(PKGDIR)/opkg/monkey/CONTROL
	@mkdir -p $(PKGDIR)/opkg/monkey/etc/ld.so.conf.d
	@mkdir -p $(PKGDIR)/opkg/monkey/etc/init.d
	@mkdir -p $(PKGDIR)/opkg/monkey/{etc/monkey/sites,etc/monkey/plugins/auth}
	@cp -ar $(MONKEY_BLDDIR)/* $(PKGDIR)/opkg/monkey/
	@chown -R root.root $(PKGDIR)/opkg/monkey/
	@rm -rf $(PKGDIR)/opkg/monkey/home/httpd/monkey/*
	@cp $(SRCDIR)/monkey/users.mk $(PKGDIR)/opkg/monkey/etc/monkey/plugins/auth/pibox.mk.pkg
	@cp $(SRCDIR)/monkey/default $(PKGDIR)/opkg/monkey/etc/monkey/sites/
	@cp $(SRCDIR)/monkey/monkey.conf $(PKGDIR)/opkg/monkey/etc/monkey/
	@cp $(SRCDIR)/monkey/plugins.load $(PKGDIR)/opkg/monkey/etc/monkey/
	@cp $(SRCDIR)/scripts/S90monkey $(PKGDIR)/opkg/monkey/etc/init.d
	@cp $(SRCDIR)/opkg/control $(PKGDIR)/opkg/monkey/CONTROL/control
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/monkey/CONTROL/postinst
	@cp $(SRCDIR)/opkg/prerm $(PKGDIR)/opkg/monkey/CONTROL/prerm
	@cp $(SRCDIR)/opkg/debian-binary $(PKGDIR)/opkg/monkey/CONTROL/debian-binary
	@sed -i 's%\[VERSION\]%'`cat $(TOPDIR)/version.txt`'%' $(PKGDIR)/opkg/monkey/CONTROL/control
	@rm -f $(PKGDIR)/opkg/monkey/etc/monkey/plugins/auth/monkey.users
	@chmod +x $(PKGDIR)/opkg/monkey/CONTROL/postinst
	@chmod +x $(PKGDIR)/opkg/monkey/CONTROL/prerm
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O monkey
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@rm -rf $(PKGDIR)/opkg

# Clean the packaging
$(MONKEY_T)-pkg-clean:
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm -rf $(PKGDIR); fi

# Clean out a cross compiler build but not the CT-NG package build.
$(MONKEY_T)-clean: $(MONKEY_T)-pkg-clean
	@cd $(MONKEY_SRCDIR) && make distclean
	@rm -f .$(MONKEY_T) 

# Clean out everything associated with MONKEY
$(MONKEY_T)-clobber: $(MONKEY_T)-pkg-clean
	@rm -rf $(BLDDIR) 
	@rm -f .$(MONKEY_T)-config .$(MONKEY_T)-init .$(MONKEY_T)-patch \
		.$(MONKEY_T)-unpack .$(MONKEY_T)-get .$(MONKEY_T)-get-patch
	@rm -f .$(MONKEY_T) 

